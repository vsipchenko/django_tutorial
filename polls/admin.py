from django.contrib import admin

from .models import Choice, Question


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['text']
    list_per_page = 1
    fieldsets = [
        ('Question text', {'fields': ['text']}),
    ]
    inlines = [ChoiceInline]


admin.site.register(Question, QuestionAdmin)
